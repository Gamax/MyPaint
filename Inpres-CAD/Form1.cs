﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyGraphicComponentLib;
using System.IO;

namespace Inpres_CAD
{
    public partial class Form1 : Form
    {

        private MyGraphicsData graphicsData = new MyGraphicsData();

        private ModeSelection _modeSelection = ModeSelection.SelectMode;

        private MyShape _shapetemp;

        private bool IsFilled = false;

        private Color _currentColor = new Color();


        private enum ModeSelection
        {
            Circle,
            Rectangle,
            Square,
            SelectMode,
            MoveMode
        };

        public Form1()
        {
            InitializeComponent();
            _currentColor = Color.Black;
            toolStripColorButton.BackColor = _currentColor;
            listBox1.DataSource = graphicsData.Shapelist;
            listBox1.DisplayMember = null;
        }

        #region Button and Menu

        #region Menu

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Voulez-vous vraiment effacer le dessin ?", "Inpres-CAD", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (result == DialogResult.OK)
                graphicsData.Shapelist.Clear();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream f;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Fichier xml (*.xml)|*.xml";
            //saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((f = saveFileDialog1.OpenFile()) != null)
                {
                    graphicsData.SaveToXML(f);
                    f.Close();
                }
            }


            
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Stream f;

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Fichier xml (*.xml)|*.xml";
            //openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                    if ((f = openFileDialog1.OpenFile()) != null)
                    {

                    graphicsData.LoadFromXML(f);
                    pictureBox1.Invalidate();
                    listBox1.DataSource = null;
                    listBox1.DataSource = graphicsData.Shapelist;
                    propertyGrid1.SelectedObject = null;
                
                    }
            }

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Voulez-vous vraiment quitter ? Tout dessin non sauvegardé sera perdu... ", "Inpres-CAD", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (result == DialogResult.OK)
                this.Close();
        }

        private void optionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OptionForm optionForm = new OptionForm();
            optionForm.ListColor = listBox1.ForeColor;
            optionForm.FontColorChange += FontColorChanged;
            optionForm.Show();

        }

        private void aboutBoxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox tmp = new AboutBox();
            tmp.Show();
        }

        #endregion

        #region Toolstrip Menu

        private void cercleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.toolStripDropDownButton1.Image = new Bitmap(Inpres_CAD.Properties.Resources.icons8_ellipse_16);
            _modeSelection = ModeSelection.Circle;
        }

        private void rectangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.toolStripDropDownButton1.Image = new Bitmap(Inpres_CAD.Properties.Resources.icons8_rectangle_16);
            _modeSelection = ModeSelection.Rectangle;
        }

        private void carréToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.toolStripDropDownButton1.Image = new Bitmap(Inpres_CAD.Properties.Resources.icons8_unchecked_checkbox_16);
            _modeSelection = ModeSelection.Square;
        }

        private void selectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.toolStripDropDownButton1.Image = new Bitmap(Inpres_CAD.Properties.Resources._181386_200);
            _modeSelection = ModeSelection.SelectMode;
        }

        #endregion

        #region ToolStrip

        private void toolStripColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog1 = new ColorDialog();
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                _currentColor = colorDialog1.Color;
                toolStripColorButton.BackColor = colorDialog1.Color;
            }            
        }

        private void toolStripFilledButton_Click_1(object sender, EventArgs e)
        {
            if (IsFilled == false)
            {
                IsFilled = true;
                toolStripFilledButton.Checked = true;
            }
            else
            {
                IsFilled = false;
                toolStripFilledButton.Checked = false;
            }
        }

        private void toolStripMoveButton_Click(object sender, EventArgs e)
        {
            if (_modeSelection == ModeSelection.SelectMode)
            {
                _modeSelection = ModeSelection.MoveMode;
                toolStripMoveButton.Checked = true;
            }
            else
            if (_modeSelection == ModeSelection.MoveMode)
            {
                _modeSelection = ModeSelection.SelectMode;
                toolStripMoveButton.Checked = false;
            }
        }

        private void toolStripDeleteButton_Click(object sender, EventArgs e)
        {
            if (graphicsData.Shapelist.Count == 0 || listBox1.SelectedIndex >= graphicsData.Shapelist.Count || listBox1.SelectedIndex < 0)
                return;

            DialogResult result = MessageBox.Show("Etes vous sûr de vouloir supprimer la forme " + graphicsData.Shapelist[listBox1.SelectedIndex].Name + "?", "Inpres-CAD", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

            if (result == DialogResult.OK)
            {
                graphicsData.Shapelist.RemoveAt(listBox1.SelectedIndex);
                propertyGrid1.SelectedObject = null;
            }
        }

        #endregion

        #endregion

        #region Picture Box

        private void pictureBox1_Paint(object sender, PaintEventArgs e) //e est de type graphics
        {
            foreach (MyShape shapetemp in graphicsData.Shapelist)
            {
                shapetemp.Draw(e.Graphics);
            }
            if (_shapetemp != null)
                _shapetemp.Draw(e.Graphics);
            pictureBox1.Invalidate();
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_shapetemp != null)
            {
                switch (_modeSelection)
                {
                    case ModeSelection.Circle:
                        ((MyCircle)_shapetemp).Radius = MyMathLib.Distance(_shapetemp.Point.x, _shapetemp.Point.y, e.X, e.Y) / 2;
                        pictureBox1.Invalidate();
                        break;
                    case ModeSelection.Rectangle:
                        ((MyRectangle)_shapetemp).Width = Math.Abs(_shapetemp.Point.x - e.X);
                        ((MyRectangle)_shapetemp).Height = Math.Abs(_shapetemp.Point.y - e.Y);
                        pictureBox1.Invalidate();
                        break;
                    case ModeSelection.Square:
                        ((MySquare)_shapetemp).Width = Math.Abs(_shapetemp.Point.x - e.X);
                        pictureBox1.Invalidate();
                        break;
                    case ModeSelection.MoveMode:
                        _shapetemp.Point = _shapetemp.getAnchorFromCenter(e.X, e.Y);
                        break;

                }
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (_shapetemp != null && _modeSelection != ModeSelection.MoveMode)
            {
                graphicsData.Shapelist.Add(_shapetemp);

            }

            _shapetemp = null;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {

            switch (_modeSelection)
            {
                case ModeSelection.Circle:
                    _shapetemp = new MyCircle(new MyPoint(e.X, e.Y), 0);
                    _shapetemp.ShapeColor = _currentColor;
                    _shapetemp.Filled = IsFilled;
                    break;
                case ModeSelection.Rectangle:
                    _shapetemp = new MyRectangle(new MyPoint(e.X, e.Y), 0, 0);
                    _shapetemp.ShapeColor = _currentColor;
                    _shapetemp.Filled = IsFilled;
                    break;
                case ModeSelection.Square:
                    _shapetemp = new MySquare(new MyPoint(e.X, e.Y), 0);
                    _shapetemp.ShapeColor = _currentColor;
                    _shapetemp.Filled = IsFilled;
                    break;
                case ModeSelection.SelectMode:

                    int count = 0;

                    foreach (MyShape shapetemp in graphicsData.Shapelist)
                    {


                        if (shapetemp.IsPointIn(new MyPoint(e.X, e.Y))) //le point cliqué est dans une forme
                        {
                            listBox1.SelectedIndex = count;
                        }
                        count++;
                    }

                    break;
                case ModeSelection.MoveMode:

                    if (graphicsData.Shapelist.Count == 0 || listBox1.SelectedIndex >= graphicsData.Shapelist.Count || listBox1.SelectedIndex < 0)
                        return;

                    _shapetemp = graphicsData.Shapelist[listBox1.SelectedIndex];

                    break;

            }




        }

        #endregion

        #region List and Properties

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < graphicsData.Shapelist.Count && listBox1.SelectedIndex > -1)
            {
                propertyGrid1.SelectedObject = listBox1.SelectedItem;
            }
            else
            {
                propertyGrid1.SelectedObject = null;
            }
        }

        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            listBox1.DisplayMember = "Name";
            listBox1.DisplayMember = null;
        }


        #endregion

        public void FontColorChanged(Color c)
        {
            listBox1.ForeColor = c;
        }
    }
}
