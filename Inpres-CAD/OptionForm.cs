﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inpres_CAD
{
    public partial class OptionForm : Form
    {
        private Color _listColor;
        public delegate void FontColorEvent(Color c);
        public event FontColorEvent FontColorChange;

        public Color ListColor
        {
            get { return _listColor; }
            set
            {
                _listColor = value;
                ColorButton.BackColor = _listColor;
            }
        }

        public OptionForm()
        {
            InitializeComponent();
        }


        private void ColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog1 = new ColorDialog();
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                _listColor = colorDialog1.Color;
                ColorButton.BackColor = _listColor;
            
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            FontColorChange(ListColor);
            Close();
        }

        private void CancelButton1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            FontColorChange(ListColor);
        }
    }
}
