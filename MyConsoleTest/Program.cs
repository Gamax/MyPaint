﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGraphicComponentLib;

namespace MyConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {

            /**
             * CREATION ET AFFICHAGE FORMES
             * */

            Console.WriteLine("AFFICHAGE DES FORMES : ");

            //Point

            MyPoint p1 = new MyPoint();
            p1.x = 10;
            p1.y = 20;
            Console.WriteLine(p1.ToString());

            MyPoint p2 = new MyPoint(20, 10);
            Console.WriteLine(p2.ToString());

            //Square

            MySquare s1 = new MySquare();
            s1.Width = 5;
            s1.Pos = p1;
            s1.Draw();

            MySquare s2 = new MySquare(p2, 5);
            s2.Draw();

            //Rectangle

            MyRectangle r1 = new MyRectangle();
            r1.Width = 5;
            r1.Height = 10;
            r1.Pos = p1;
            r1.Draw();


            MyRectangle r2 = new MyRectangle(p1,5,10);
            r2.Draw();

            //Circle

            MyCircle c1 = new MyCircle();
            c1.Pos = p2;
            c1.Radius = 5;
            c1.Draw();

            MyCircle c2 = new MyCircle(p1, 7);
            c2.Draw();

            /**
             * LISTES :
             */

            List<MyShape> Liste = new List<MyShape>();

            Liste.Add(s1);
            Liste.Add(s2);
            Liste.Add(r1);
            Liste.Add(r2);
            Liste.Add(c1);
            Liste.Add(c2);

            Console.WriteLine("AFFICHAGE DE LA LISTE : ");

            foreach(MyShape element in Liste)
            {
                element.Draw();
            }

            Console.WriteLine("AFFICHAGE DE LA LISTE (IPointy) : ");

            foreach (MyShape element in Liste)
            {
                if(element is IPointy)
                    element.Draw();
            }

            Console.WriteLine("AFFICHAGE DE LA LISTE (!IPointy) : ");

            foreach (MyShape element in Liste)
            {
                if(!(element is IPointy))
                    element.Draw();
            }

            Console.WriteLine("AFFICHAGE DE LA LISTE TRIEE PAR AIRE : ");

            Liste.Sort();

            foreach (MyShape element in Liste)
            {
                element.Draw();
            }
           

            Console.ReadKey();

            int choix;

            while (true)
            {
                Console.WriteLine("MENU : ");
                Console.WriteLine("1) Square");
                Console.WriteLine("2) Rectangle");
                Console.WriteLine("3) Circle");
                Console.WriteLine("0) Exit");
                Console.WriteLine("> ");
                choix = Int32.Parse(Console.ReadLine());

                switch (choix)
                {
                    case 1:
                        Square();
                        break;
                    case 2:
                        Rectangle();
                        break;
                    case 3:
                        Circle();
                        break;
                    case 0:
                        return;
                }
            }

           
        }

        static void Square()
        {
            //LISTE triée carré

            Console.WriteLine("AFFICHAGE DE LA LISTESQUARE TRIEE : ");

            List<MySquare> ListeSquare = new List<MySquare>(); //todo List<MyShape>

            ListeSquare.Add(new MySquare(new MyPoint(12, 3), 20));
            ListeSquare.Add(new MySquare(new MyPoint(-5, 4), 10));
            ListeSquare.Add(new MySquare(new MyPoint(5, 8), 15));
            ListeSquare.Add(new MySquare(new MyPoint(-4, 8), 15));
            ListeSquare.Add(new MySquare(new MyPoint(7, 6), 9));

            ListeSquare.Sort();

            foreach (MySquare element in ListeSquare)
            {
                element.Draw();
            }

            //LISTE triée carré abscisse

            Console.WriteLine("AFFICHAGE DE LA LISTESQUARE TRIEE ABSCISSE: ");

            ListeSquare.Sort(new MySquareAbscisseComparer());

            foreach (MySquare element in ListeSquare)
            {
                element.Draw();
            }

            int width;

            Console.Write("Entrez la taille du carré de référence : ");
            width = Int32.Parse(Console.ReadLine());

            MySquare temp = new MySquare(new MyPoint(0, 0), width);

            List<MySquare> templist = ListeSquare.FindAll(e => e.Equals(temp));

            Console.WriteLine("Affichage liste carré de taille de référence : ");

            foreach (MySquare element in templist)
            {
                element.Draw();
            }

            MyPoint ptemp = new MyPoint();
            Console.WriteLine("Entrez le point : ");
            Console.Write("X : ");
            ptemp.x = Int32.Parse(Console.ReadLine());
            Console.Write("Y : ");
            ptemp.y = Int32.Parse(Console.ReadLine());

            templist = ListeSquare.FindAll(e => e.IsPointIn(ptemp));

            Console.WriteLine("Affichage liste carré contenant le point : ");

            foreach (MySquare element in templist)
            {
                element.Draw();
            }
  
        }

        static void Rectangle()
        {
            //LISTE triée rectangle

            Console.WriteLine("AFFICHAGE DE LA LISTERECTANGLE TRIEE : ");

            List<MyRectangle> ListeRectangle = new List<MyRectangle>(); //todo List<MyShape>

            ListeRectangle.Add(new MyRectangle(new MyPoint(2, 3), 20, 10));
            ListeRectangle.Add(new MyRectangle(new MyPoint(10, 4), 10,5));
            ListeRectangle.Add(new MyRectangle(new MyPoint(21, 11), 15,20));
            ListeRectangle.Add(new MyRectangle(new MyPoint(5, 8), 15,20));
            ListeRectangle.Add(new MyRectangle(new MyPoint(7, 4), 9,14));

            ListeRectangle.Sort();

            foreach (MyRectangle element in ListeRectangle)
            {
                element.Draw();
            }

            //LISTE triée rectangle abscisse

            Console.WriteLine("AFFICHAGE DE LA LISTERECTANGLE TRIEE ABSCISSE: ");

            ListeRectangle.Sort(new MyRectangleAbscisseComparer());

            foreach (MyRectangle element in ListeRectangle)
            {
                element.Draw();
            }

            int width,height;

            Console.WriteLine("Entrez la taille du rectangle de référence : ");
            Console.Write("Width : ");
            width = Int32.Parse(Console.ReadLine());
            Console.Write("Height : ");
            height = Int32.Parse(Console.ReadLine());

            MyRectangle temp = new MyRectangle(new MyPoint(0, 0), height, width);

            List<MyRectangle> templist = ListeRectangle.FindAll(e => e.Equals(temp));

            Console.WriteLine("Affichage liste rectangle de taille de référence : ");

            foreach (MyRectangle element in templist)
            {
                element.Draw();
            }

            MyPoint ptemp = new MyPoint();
            Console.WriteLine("Entrez le point : ");
            Console.Write("X : ");
            ptemp.x = Int32.Parse(Console.ReadLine());
            Console.Write("Y : ");
            ptemp.y = Int32.Parse(Console.ReadLine());

            templist = ListeRectangle.FindAll(e => e.IsPointIn(ptemp));

            Console.WriteLine("Affichage liste rectangle contenant le point : ");

            foreach (MyRectangle element in templist)
            {
                element.Draw();
            }
        }

        static void Circle()
        {
            //LISTE triée cercle

            Console.WriteLine("AFFICHAGE DE LA LISTECIRCLE TRIEE : ");

            List<MyCircle> ListeCircle = new List<MyCircle>(); //todo List<MyShape>

            ListeCircle.Add(new MyCircle(new MyPoint(12, 3), 20));
            ListeCircle.Add(new MyCircle(new MyPoint(-5, 4), 10));
            ListeCircle.Add(new MyCircle(new MyPoint(5, 8), 15));
            ListeCircle.Add(new MyCircle(new MyPoint(-4, 8), 15));
            ListeCircle.Add(new MyCircle(new MyPoint(7, 6), 9));

            ListeCircle.Sort();

            foreach (MyCircle element in ListeCircle)
            {
                element.Draw();
            }

            //LISTE triée carré abscisse

            Console.WriteLine("AFFICHAGE DE LA LISTECIRCLE TRIEE ABSCISSE: ");

            ListeCircle.Sort(new MyCircleAbscisseComparer());

            foreach (MyCircle element in ListeCircle)
            {
                element.Draw();
            }

            int radius;

            Console.Write("Entrez le rayon du cercle de référence : ");
            radius = Int32.Parse(Console.ReadLine());

            MyCircle temp = new MyCircle(new MyPoint(0, 0), radius);

            List<MyCircle> templist = ListeCircle.FindAll(e => e.Equals(temp));

            Console.WriteLine("Affichage liste cercle de taille de référence : ");

            foreach (MyCircle element in templist)
            {
                element.Draw();
            }

            MyPoint ptemp = new MyPoint();
            Console.WriteLine("Entrez le point : ");
            Console.Write("X : ");
            ptemp.x = Int32.Parse(Console.ReadLine());
            Console.Write("Y : ");
            ptemp.y = Int32.Parse(Console.ReadLine());

            templist = ListeCircle.FindAll(e => e.IsPointIn(ptemp));

            Console.WriteLine("Affichage liste cercle contenant le point : ");

            foreach (MyCircle element in templist)
            {
                element.Draw();
            }
        }
    }
}
