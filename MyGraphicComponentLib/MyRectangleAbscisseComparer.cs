﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGraphicComponentLib
{
    public class MyRectangleAbscisseComparer : IComparer<MyRectangle>
    {
        public int Compare(MyRectangle r1, MyRectangle r2)
        {
            return r1.Pos.x.CompareTo(r2.Pos.x);
        }
    }
}
