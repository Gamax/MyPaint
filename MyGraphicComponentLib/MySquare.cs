﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace MyGraphicComponentLib
{
    [Serializable]
    public class MySquare : MyShape, IIsPointIn, IPointy, IComparable<MySquare>, IEquatable<MySquare>
    {

        private int _width;
        private static int _cpt;

        public override event PropertyChangedEventHandler PropertyChanged;

        #region Properties

        [DescriptionAttribute("Taille du côté du carré"), CategoryAttribute("Taille et position")]
        public int Width
        {
            get { return _width; }
            set { _width = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Width"));
                }
            }
        }

        [DescriptionAttribute("Nombre de carré instancié"), CategoryAttribute("Définition")]
        public int Cpt
        {
            get { return _cpt; }
        }


        [BrowsableAttribute(false)]
        public MyPoint Pos
        {
            get { return Point; }
            set { Point = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Pos"));
                }
            }
        }

        int IPointy.Points
        {
            get { return 4; }
        }

        #endregion

        public MySquare()
        {
            _width = 0;
            _cpt++;
            Name = "S" + _cpt;
        }

        public MySquare(MyPoint point, int width)
        {
            Point = point;
            Width = width;
            _cpt++;
            Name = "S" + _cpt;

        }

        public override string ToString()
        {
            return Name + " : " + Point.ToString() +"\tF : " + Filled + "\t" + ShapeColor + "\tW : " + Width ;
        }

        public override void Draw(Graphics g)
        {
            Console.WriteLine(ToString());
            if (Filled)
            {
                g.FillRectangle(new SolidBrush(ShapeColor), new Rectangle(Point.x, Point.y, Width, Width));            }
            else
            {
                g.DrawRectangle(new Pen(ShapeColor), Point.x, Point.y, Width, Width);
            }
        }

        public override bool IsPointIn(MyPoint p)
        {
            if (p.x < Point.x + Width && p.x > Point.x && p.y < Point.y + Width && p.y > Point.y)
                return true;
            return false;
        }

        public int CompareTo(MySquare other)
        {
            return this.Width.CompareTo(other.Width);
        }

        public bool Equals(MySquare other)
        {
            return Width.Equals(other.Width);
        }

        public override double Area()
        {
            return Math.Pow(Width, 2);
        }

        public override MyPoint getAnchorFromCenter(int x, int y)
        {
            return new MyPoint(x - Width / 2, y - Width / 2);
        }

    }
}
