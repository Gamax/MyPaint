﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace MyGraphicComponentLib
{
    [Serializable]
    public class MyRectangle : MyShape, IIsPointIn, IPointy, IComparable<MyRectangle>, IEquatable<MyRectangle>
    {

        private int _height;
        private int _width;
        private static int _cpt;

        public override event PropertyChangedEventHandler PropertyChanged;

        #region Properties

        [DescriptionAttribute("Hauteur du rectangle"), CategoryAttribute("Taille et position")]
        public int Height
        {
            get { return _height; }
            set { _height = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Height"));
                }
            }
        }

        [DescriptionAttribute("Largeur du rectangle"), CategoryAttribute("Taille et position")]
        public int Width
        {
            get { return _width; }
            set { _width = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Width"));
                }
            }
        }

        [BrowsableAttribute(false)]

        public MyPoint Pos
        {
            get { return Point; }
            set { Point = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Pos"));
                }
            }
        }

        [BrowsableAttribute(false)]
        int IPointy.Points
        {
            get { return 4; }
        }

        [DescriptionAttribute("Nombre de rectangle instancié"), CategoryAttribute("Définition")]
        public int Cpt
        {
            get { return _cpt; }
        }
        #endregion

        public MyRectangle()
        {
            Height = 0;
            Width = 0;
            _cpt++;
            Name = "R" + _cpt;
        }

        public MyRectangle(MyPoint point,int height, int width)
        {
            Point = point;
            Height = height;
            Width = width;
            _cpt++;
            Name = "R" + _cpt;
        }

        public override string ToString()
        {
            return Name + " : " + Point.ToString() + "\tF : " + Filled + "\t" + ShapeColor + "\tH : " + Height + "\tW : " + Width;
        }

        public override void Draw(Graphics g)
        {
            if (Filled)
            {
                g.FillRectangle(new SolidBrush(ShapeColor), new Rectangle(Point.x, Point.y, Width, Height));
            }
            else
            {
                g.DrawRectangle(new Pen(ShapeColor), Point.x, Point.y, Width, Height);
            }

            
            Console.WriteLine(ToString());
        }

        public override bool IsPointIn(MyPoint p)
        {
            if (p.x < Point.x + Width && p.x > Point.x && p.y < Point.y + Height && p.y > Point.y)
                return true;
            return false;
        }

        public int CompareTo(MyRectangle other)
        {
            return (this.Width * this.Height).CompareTo(other.Width*other.Height);
        }

        public bool Equals(MyRectangle other)
        {
            return Width.Equals(other.Width) && Height.Equals(other.Height);
        }

        public override double Area()
        {
            return Width*Height;
        }

        public override MyPoint getAnchorFromCenter(int x, int y)
        {
            return new MyPoint(x - Width / 2, y - Height / 2);
        }



    }
}
