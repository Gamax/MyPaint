﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;
using System.Xml.Serialization;

namespace MyGraphicComponentLib
{
    //[XmlInclude(typeof(MySquare))]
    //[XmlInclude(typeof(MyRectangle))]
    //[XmlInclude(typeof(MyCircle))]
    [Serializable]
    public abstract class  MyShape : IComparable<MyShape>, IDrawable, IIsPointIn, INotifyPropertyChanged
    {

        private MyPoint _point;
        private String _name;
        private bool _filled;
        private Color _color;
        public virtual event PropertyChangedEventHandler PropertyChanged;

        #region Properties


        [XmlIgnore,DescriptionAttribute("Couleur de la forme"), CategoryAttribute("Définition")]
        public Color ShapeColor
        {
            get { return _color; }
            set { _color = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Col"));
                }
            }
        }

        [XmlElement("Col"), BrowsableAttribute(false), DescriptionAttribute("La couleur de la forme")]
        public int ColorAsArgb
        {
            get { return ShapeColor.ToArgb(); }
            set { ShapeColor = Color.FromArgb(value); }
        }

        [DescriptionAttribute("Détermine si la forme est pleine ou vide"), CategoryAttribute("Définition")]
        public bool Filled
        {
            get { return _filled; }
            set { _filled = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Filled"));
                }
            }
        }

        [DescriptionAttribute("Point d'ancrage de la forme"), CategoryAttribute("Taille et position")]
        public MyPoint Point {
            get { return _point; }
            set { _point = value;
            }
        }

        [DescriptionAttribute("Nom de la forme"), CategoryAttribute("Définition")]
        public String Name
        {
            get { return _name; }
            set { _name = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        #endregion

        public MyShape()
        {
            ShapeColor = Color.Orange;
            Filled = false;
        }

        public int CompareTo(MyShape other)
        {
            return this.Area().CompareTo(other.Area());
        }

        public abstract void Draw(Graphics g);

        public abstract double Area();

        public abstract bool IsPointIn(MyPoint p);

        public abstract MyPoint getAnchorFromCenter(int x, int y);
    }
}
