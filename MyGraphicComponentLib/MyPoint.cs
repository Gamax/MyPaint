﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace MyGraphicComponentLib
{
    public class MyPoint : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        private int _x;
        private int _y;

        public int x
        {
            get { return _x; }
            set { _x = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("x"));
                }
            }
        }

        public int y
        {
            get { return _y; }
            set { _y = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("y"));
                }
            }
        }

        public MyPoint()
        {
            _x = 0;
            _y = 0;
        }

        public MyPoint(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public override string ToString()
        {
            return "(" + _x + "," + _y + ")";
        }



    }
}
