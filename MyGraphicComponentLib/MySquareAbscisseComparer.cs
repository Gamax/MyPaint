﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGraphicComponentLib
{
    public class MySquareAbscisseComparer : IComparer<MySquare>
    {
        public int Compare(MySquare s1, MySquare s2)
        {
            return s1.Pos.x.CompareTo(s2.Pos.x);
        }
    }
}
