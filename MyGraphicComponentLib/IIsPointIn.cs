﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGraphicComponentLib
{
    public interface IIsPointIn
    {

        bool IsPointIn(MyPoint p);

    }
}
