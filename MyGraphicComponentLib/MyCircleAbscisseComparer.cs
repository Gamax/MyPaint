﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGraphicComponentLib
{
    public class MyCircleAbscisseComparer : IComparer<MyCircle>
    {
        public int Compare(MyCircle c1, MyCircle c2)
        {
            return c1.Pos.x.CompareTo(c2.Pos.x);
        }
    }
}
