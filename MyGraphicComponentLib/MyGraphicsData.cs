﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Xml.Serialization;
using System.IO;

namespace MyGraphicComponentLib
{
    public class MyGraphicsData
    {

        private BindingList<MyShape> _shapelist = new BindingList<MyShape>();

        public BindingList<MyShape> Shapelist
        {
            get { return _shapelist; }
        }

        public void reset()
        {
            _shapelist.Clear();
        }

        public void SaveToXML(Stream f)
        {
            Type[] shapeTypes = { typeof(MySquare), typeof(MyRectangle),typeof(MyCircle) };
            XmlSerializer xmlFormat = new XmlSerializer(typeof(BindingList<MyShape>), shapeTypes);
            using (f)
            {
                xmlFormat.Serialize(f, _shapelist);
            }

        }

        public void LoadFromXML(Stream f)
        {
            Type[] shapeTypes = { typeof(MySquare), typeof(MyRectangle), typeof(MyCircle) };
            XmlSerializer xmlFormat = new XmlSerializer(typeof(BindingList<MyShape>), shapeTypes);
            using (f)
            {
                _shapelist = (BindingList<MyShape>)xmlFormat.Deserialize(f);
            }

        }
        

    }
}
