﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace MyGraphicComponentLib
{
    [Serializable]
    public class MyCircle : MyShape, IIsPointIn,IComparable<MyCircle>,IEquatable<MyCircle>
    {

        private int _radius;
        private static int _cpt;

        public override event PropertyChangedEventHandler PropertyChanged;

        #region Properties

        [DescriptionAttribute("Rayon du cercle"), CategoryAttribute("Taille et position")]
        public int Radius
        {
            get { return _radius; }
            set { _radius = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Radius"));
                }
            }
        }

        [BrowsableAttribute(false)]
        public MyPoint Pos
        {
            get { return Point; }
            set { Point = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Pos"));
                }
            }
        }

        [DescriptionAttribute("Nombre de cercle instancié"), CategoryAttribute("Définition")]
        public int Cpt
        {
            get { return _cpt; }
        }
        #endregion

        public MyCircle()
        {
            Radius = 0;
            _cpt++;
            Name = "C" + _cpt;
        }

        public MyCircle(MyPoint Point, int radius)
        {
            this.Point = Point;
            this.Radius = radius;
            _cpt++;
            Name = "C" + _cpt;
        }

        public override string ToString()
        {
            return Name + " : " + Point.ToString() + "\tF : " + Filled + "\t" + ShapeColor + "\tR : " + Radius;
        }

        public override void Draw(Graphics g)
        {
            Console.WriteLine(ToString());
            if (Filled)
            {
                g.FillEllipse(new SolidBrush(ShapeColor), new Rectangle(Point.x, Point.y, Radius * 2, Radius * 2));
            }
            else
            {
                g.DrawEllipse(new System.Drawing.Pen(ShapeColor), Point.x, Point.y, Radius * 2, Radius * 2);
            }
            
        }

        public override bool IsPointIn(MyPoint p)
        {
            if (Math.Pow(p.x-(Point.x + Radius),2) + Math.Pow(p.y- (Point.y + Radius), 2) < Math.Pow(Radius,2))
                return true;
            return false;
        }

        public int CompareTo(MyCircle other)
        {
            return this.Radius.CompareTo(other.Radius);
        }

        public bool Equals(MyCircle other)
        {
            return Radius.Equals(other.Radius);
        }

        public override double Area()
        {
            return Math.Pow(Radius, 2) * Math.PI;
        }

        public override MyPoint getAnchorFromCenter(int x, int y)
        {
            return new MyPoint(x - Radius, y - Radius);
        }


    }
}
